# frozen_string_literal: true

require './spec/spec_helper'

RSpec.describe TanukiEmoji::Character do
  subject do
    described_class.new('horse',
                        codepoints: "\u{1f434}",
                        alpha_code: 'horse',
                        description: 'horse face',
                        category: 'nature')
  end
  let(:mage_emoji) do
    described_class.new('mage',
                        codepoints: "\u{1f9d9}\u{1f3fb}",
                        alpha_code: ':mage_tone1:',
                        description: 'mage with lighter skin',
                        category: 'people')
  end
  let(:brazillian_flag) do
    described_class.new('flag_br',
                        codepoints: "\u{1f1e7}\u{1f1f7}",
                        alpha_code: ':flag_br:',
                        description: 'brazil',
                        category: 'flags')
  end

  describe '#initialize' do
    it 'formats the name' do
      emoji = described_class.new(':mage:',
                                  codepoints: "\u{1f9d9}\u{1f3fb}",
                                  alpha_code: ':mage_tone1:',
                                  description: 'mage with lighter skin',
                                  category: 'people')

      expect(emoji.name).to eq('mage')
    end

    it 'formats the alpha_code' do
      emoji = described_class.new(':mage:',
                                  codepoints: "\u{1f9d9}\u{1f3fb}",
                                  alpha_code: 'mage_tone1',
                                  description: 'mage with lighter skin',
                                  category: 'people')

      expect(emoji.alpha_code).to eq(':mage_tone1:')
    end
  end

  describe '#category' do
    it 'returns category associated with emoji' do
      expect(subject.category).to eq('nature')
    end
  end

  context '#inspect' do
    it 'returns a formatted string' do
      expect(subject.inspect).to eq('#<TanukiEmoji::Character:horse 🐴(1f434)>')
    end
  end

  context '#to_s' do
    it 'returns the codepoints' do
      expect(subject.to_s).to eq subject.codepoints
    end
  end

  context '#add_codepoints' do
    it 'adds codepoints to codepoints_alternate' do
      expect { subject.add_codepoints("\u{1f434}\u{fe0f}") }.to change { subject.codepoints_alternates.count }.by(1)
      expect(subject.codepoints_alternates).to include("\u{1f434}\u{fe0f}")
    end
  end

  context '#add_alias' do
    it 'adds a alpha_code to aliases' do
      expect { subject.add_alias(':horse_face:') }.to change { subject.aliases.count }.by(1)
      expect(subject.aliases).to include(':horse_face:')
    end

    it 'converts `shortname` to `:alpha_code:` when inserting' do
      subject.add_alias('horse_face')

      expect(subject.aliases).to include(':horse_face:')
    end
  end

  context '#add_ascii_alias' do
    it 'adds an ASCII alias to ascii_aliases' do
      expect { subject.add_ascii_alias(':)') }.to change { subject.ascii_aliases.count }.by(1)
      expect(subject.ascii_aliases).to include(':)')
    end
  end

  context '#hex' do
    context 'with a character with a single codepoint' do
      it 'returns a formatted hex representation of the codepoint' do
        expect(subject.hex).to eq('1f434')
      end
    end

    context 'with a character with multiple codepoints' do
      it 'returns a formatted hex representation of the multiple codepoints separated by dash' do
        expect(mage_emoji.hex).to eq('1f9d9-1f3fb')
      end
    end
  end

  context '#image_name' do
    context 'with all indexed emojis' do
      let(:noto_emoji_base_path) { File.expand_path(File.join(__dir__, '../../vendor/noto-emoji')) }
      let(:noto_emoji_path) { File.join(noto_emoji_base_path, 'png/32') }
      let(:noto_flag_path) { File.join(noto_emoji_base_path, 'third_party/region-flags/png') }

      before do
        expect(Dir).to exist(noto_emoji_path), 'Please run `bin/setup`'
      end

      it 'name matches an existing image file from vendored resources', :aggregate_failures do
        missing_files = TanukiEmoji.index.all.reject do |emoji|
          if emoji.flag?
            File.exist?(File.join(noto_flag_path, emoji.image_name))
          else
            File.exist?(File.join(noto_emoji_path, emoji.image_name))
          end
        end

        expect(missing_files.count).to eq(0)
        expect(missing_files).to be_empty
      end

      it 'name matches an existing image imported into images_path folder', :aggregate_failures do
        images_path = TanukiEmoji.images_path

        missing_files = TanukiEmoji.index.all.reject do |emoji|
          File.exist?(File.join(images_path, emoji.image_name))
        end

        expect(missing_files.count).to eq(0)
        expect(missing_files).to be_empty
      end
    end
  end

  context '#flag?' do
    it 'returns true when character is made of flag-like A-Z characters' do
      expect(brazillian_flag.flag?).to be_truthy
    end

    it 'returns false when character is not made of flag-like A-Z characters' do
      expect(mage_emoji.flag?).to be_falsey
    end
  end

  context '#==' do
    it 'returns true for the same instance of character' do
      expect(mage_emoji).to eq(mage_emoji)
    end

    it 'returns true to different instances that share the same attributes' do
      one_horse = described_class.new('horse',
                                      codepoints: "\u{1f434}",
                                      alpha_code: 'horse',
                                      description: 'horse face',
                                      category: 'nature')

      similar_horse = described_class.new('horse',
                                          codepoints: "\u{1f434}",
                                          alpha_code: 'horse',
                                          description: 'horse face',
                                          category: 'nature')

      expect(one_horse).to eq(similar_horse)
    end

    it 'returns false when at least one attributes is different' do
      one_horse = described_class.new('horse',
                                      codepoints: "\u{1f434}",
                                      alpha_code: 'horse',
                                      description: 'horse face',
                                      category: 'nature')

      another_horse = described_class.new('horse',
                                          codepoints: "\u{1f434}",
                                          alpha_code: 'another_horse',
                                          description: 'horse face',
                                          category: 'nature')

      expect(one_horse).not_to eq(another_horse)
    end
  end

  context '.format_alpha_code' do
    it 'adds columns when provided alpha code doesnt have it yet' do
      expect(described_class.format_alpha_code('horse')).to eq(':horse:')
    end

    it 'keeps provided alpha code unchanged when it is already surrounded by columns' do
      expect(described_class.format_alpha_code(':horse:')).to eq(':horse:')
    end

    it 'works with +1 and -1', :aggregate_failures do
      expect(described_class.format_alpha_code('+1')).to eq(':+1:')
      expect(described_class.format_alpha_code(':+1:')).to eq(':+1:')
      expect(described_class.format_alpha_code('-1')).to eq(':-1:')
      expect(described_class.format_alpha_code(':-1:')).to eq(':-1:')
    end
  end

  context '.format_name' do
    it 'removes columns when provided an alpha_code' do
      expect(described_class.format_name(':horse:')).to eq('horse')
    end

    it 'keeps provided name unchnaged when it is already in the correct format' do
      expect(described_class.format_name('horse')).to eq('horse')
    end

    it 'works with +1 and -1', :aggregate_failures do
      expect(described_class.format_name(':+1:')).to eq('+1')
      expect(described_class.format_name('+1')).to eq('+1')
      expect(described_class.format_name(':-1:')).to eq('-1')
      expect(described_class.format_name('-1')).to eq('-1')
    end
  end
end
